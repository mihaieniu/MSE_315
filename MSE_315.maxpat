{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 5,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 515.0, 144.0, 576.0, 327.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"bgcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
					"id" : "obj-6",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 546.0, 299.0, 16.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 546.0, 293.5, 16.0, 16.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.011765, 0.396078, 0.752941, 1.0 ],
					"id" : "obj-4",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 528.0, 299.0, 16.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 525.0, 293.5, 16.0, 16.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-87",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "audioTrack_reduced.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"offset" : [ -18.0, -19.0 ],
					"patching_rect" : [ 1.0, 4.5, 564.0, 310.5 ],
					"presentation" : 1,
					"presentation_rect" : [ -2.0, -1.0, 564.0, 310.5 ],
					"varname" : "audioTrack_reduced",
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595187,
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 153.0, 119.0, 77.0, 22.0 ],
					"style" : "",
					"text" : "window exec"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595187,
					"id" : "obj-53",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 298.0, 88.0, 110.0, 22.0 ],
					"style" : "",
					"text" : "window flags menu"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595187,
					"id" : "obj-52",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 153.0, 88.0, 123.0, 22.0 ],
					"style" : "",
					"text" : "window flags nomenu"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595187,
					"id" : "obj-16",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 153.0, 59.0, 119.0, 22.0 ],
					"style" : "",
					"text" : "window flags nogrow"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595187,
					"id" : "obj-28",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 298.0, 59.0, 106.0, 22.0 ],
					"style" : "",
					"text" : "window flags grow"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 44.0, 38.0, 69.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"style" : "",
					"text" : "thispatcher"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"order" : 2,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"order" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"order" : 1,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"order" : 2,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"order" : 1,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"order" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-87::obj-3486::obj-102::obj-47::obj-100" : [ "Offset[12]", "Offset", 0 ],
			"obj-87::obj-3486::obj-102::obj-48::obj-154" : [ "CV2[13]", "CV2", 0 ],
			"obj-87::obj-3486::obj-102::obj-49::obj-3" : [ "Wave[14]", "Wave", 0 ],
			"obj-87::obj-3486::obj-102::obj-29::obj-100" : [ "Offset[16]", "Offset", 0 ],
			"obj-87::obj-3977::obj-132" : [ "vst~[15]", "vst~", 0 ],
			"obj-87::obj-2::obj-5::obj-46::obj-81" : [ "Bank[27]", "Bank", 0 ],
			"obj-87::obj-226::obj-132" : [ "vst~[91]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-49::obj-110" : [ "Wavetable[14]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-102::obj-27::obj-3" : [ "Wave[18]", "Wave", 0 ],
			"obj-87::obj-3486::obj-5::obj-49::obj-100" : [ "Offset[10]", "Offset", 0 ],
			"obj-87::obj-4032::obj-132" : [ "vst~[35]", "vst~", 0 ],
			"obj-87::obj-2::obj-5::obj-26::obj-3" : [ "Wave[33]", "Wave", 0 ],
			"obj-87::obj-270::obj-22::obj-9" : [ "live.dial[18]", "live.dial", 0 ],
			"obj-87::obj-237::obj-22::obj-9" : [ "live.dial[23]", "live.dial", 0 ],
			"obj-87::obj-3486::obj-102::obj-48::obj-100" : [ "Offset[13]", "Offset", 0 ],
			"obj-87::obj-3966::obj-132" : [ "vst~[11]", "vst~", 0 ],
			"obj-87::obj-4088::obj-22::obj-9" : [ "live.dial[13]", "live.dial", 0 ],
			"obj-87::obj-2::obj-102::obj-46::obj-154" : [ "CV2[19]", "CV2", 0 ],
			"obj-87::obj-3486::obj-102::obj-49::obj-81" : [ "Bank[14]", "Bank", 0 ],
			"obj-87::obj-4021::obj-132" : [ "vst~[31]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-49::obj-81" : [ "Bank[22]", "Bank", 0 ],
			"obj-87::obj-2::obj-5::obj-48::obj-12" : [ "Mute[29]", "Mute", 0 ],
			"obj-87::obj-314::obj-134" : [ "vst~[57]", "vst~", 0 ],
			"obj-87::obj-270::obj-134" : [ "vst~[73]", "vst~", 0 ],
			"obj-87::obj-193::obj-132" : [ "vst~[99]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-46::obj-100" : [ "Offset[11]", "Offset", 0 ],
			"obj-87::obj-3955::obj-138" : [ "vst~[4]", "vst~", 0 ],
			"obj-87::obj-4076::obj-22::obj-9" : [ "live.dial[12]", "live.dial", 0 ],
			"obj-87::obj-2::obj-102::obj-26::obj-154" : [ "CV2[25]", "CV2", 0 ],
			"obj-87::obj-2::obj-102::obj-27::obj-81" : [ "Bank[26]", "Bank", 0 ],
			"obj-87::obj-2::obj-5::obj-47::obj-110" : [ "Wavetable[28]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-5::obj-28::obj-3" : [ "Wave[5]", "Wave", 0 ],
			"obj-87::obj-4010::obj-134" : [ "vst~[25]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-48::obj-81" : [ "Bank[21]", "Bank", 0 ],
			"obj-87::obj-303::obj-138" : [ "vst~[60]", "vst~", 0 ],
			"obj-87::obj-259::obj-138" : [ "vst~[76]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-27::obj-154" : [ "CV2[18]", "CV2", 0 ],
			"obj-87::obj-4065::obj-22::obj-9" : [ "live.dial[11]", "live.dial", 0 ],
			"obj-87::obj-2::obj-102::obj-29::obj-100" : [ "Offset[24]", "Offset", 0 ],
			"obj-87::obj-2::obj-102::obj-26::obj-81" : [ "Bank[25]", "Bank", 0 ],
			"obj-87::obj-2::obj-5::obj-47::obj-154" : [ "CV2[28]", "CV2", 0 ],
			"obj-87::obj-3486::obj-102::obj-48::obj-110" : [ "Wavetable[13]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-102::obj-28::obj-154" : [ "CV2[15]", "CV2", 0 ],
			"obj-87::obj-3486::obj-5::obj-49::obj-12" : [ "Mute[10]", "Mute", 0 ],
			"obj-87::obj-3999::obj-132" : [ "vst~[23]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-48::obj-100" : [ "Offset[21]", "Offset", 0 ],
			"obj-87::obj-303::obj-132" : [ "vst~[63]", "vst~", 0 ],
			"obj-87::obj-248::obj-22::obj-9" : [ "live.dial[20]", "live.dial", 0 ],
			"obj-87::obj-3486::obj-102::obj-47::obj-12" : [ "Mute[12]", "Mute", 0 ],
			"obj-87::obj-4054::obj-22::obj-9" : [ "live.dial[10]", "live.dial", 0 ],
			"obj-87::obj-2::obj-102::obj-29::obj-3" : [ "Wave[24]", "Wave", 0 ],
			"obj-87::obj-2::obj-5::obj-27::obj-81" : [ "Bank[34]", "Bank", 0 ],
			"obj-87::obj-237::obj-132" : [ "vst~[95]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-26::obj-81" : [ "Bank[17]", "Bank", 0 ],
			"obj-87::obj-3988::obj-138" : [ "vst~[16]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-47::obj-154" : [ "CV2[20]", "CV2", 0 ],
			"obj-87::obj-2::obj-5::obj-28::obj-110" : [ "Wavetable[31]", "Wavetable", 0 ],
			"obj-87::obj-215::obj-22::obj-9" : [ "live.dial[21]", "live.dial", 0 ],
			"obj-87::obj-3486::obj-5::obj-29::obj-154" : [ "CV2[6]", "CV2", 0 ],
			"obj-87::obj-3944::obj-138" : [ "vst~", "vst~", 0 ],
			"obj-87::obj-4043::obj-22::obj-9" : [ "live.dial[9]", "live.dial", 0 ],
			"obj-87::obj-2::obj-102::obj-28::obj-3" : [ "Wave[23]", "Wave", 0 ],
			"obj-87::obj-2::obj-5::obj-27::obj-154" : [ "CV2[34]", "CV2", 0 ],
			"obj-87::obj-3977::obj-133" : [ "vst~[14]", "vst~", 0 ],
			"obj-87::obj-2::obj-5::obj-46::obj-110" : [ "Wavetable[27]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-5::obj-29::obj-100" : [ "Offset[6]", "Offset", 0 ],
			"obj-87::obj-4032::obj-22::obj-9" : [ "live.dial[8]", "live.dial", 0 ],
			"obj-87::obj-2::obj-5::obj-49::obj-12" : [ "Mute[30]", "Mute", 0 ],
			"obj-87::obj-2::obj-5::obj-26::obj-12" : [ "Mute[33]", "Mute", 0 ],
			"obj-87::obj-281::obj-138" : [ "vst~[68]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-26::obj-100" : [ "Offset[17]", "Offset", 0 ],
			"obj-87::obj-3486::obj-5::obj-46::obj-154" : [ "CV2[7]", "CV2", 0 ],
			"obj-87::obj-3966::obj-133" : [ "vst~[10]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-46::obj-110" : [ "Wavetable[19]", "Wavetable", 0 ],
			"obj-87::obj-237::obj-138" : [ "vst~[92]", "vst~", 0 ],
			"obj-87::obj-4021::obj-22::obj-9" : [ "live.dial[7]", "live.dial", 0 ],
			"obj-87::obj-2::obj-102::obj-49::obj-100" : [ "Offset[22]", "Offset", 0 ],
			"obj-87::obj-314::obj-138" : [ "vst~[56]", "vst~", 0 ],
			"obj-87::obj-281::obj-132" : [ "vst~[71]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-46::obj-100" : [ "Offset[7]", "Offset", 0 ],
			"obj-87::obj-3955::obj-133" : [ "vst~[6]", "vst~", 0 ],
			"obj-87::obj-4088::obj-134" : [ "vst~[53]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-27::obj-100" : [ "Offset[26]", "Offset", 0 ],
			"obj-87::obj-215::obj-138" : [ "vst~[84]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-49::obj-3" : [ "Wave[10]", "Wave", 0 ],
			"obj-87::obj-4010::obj-22::obj-9" : [ "live.dial[6]", "live.dial", 0 ],
			"obj-87::obj-2::obj-5::obj-29::obj-110" : [ "Wavetable[32]", "Wavetable", 0 ],
			"obj-87::obj-303::obj-22::obj-9" : [ "live.dial[15]", "live.dial", 0 ],
			"obj-87::obj-270::obj-133" : [ "vst~[74]", "vst~", 0 ],
			"obj-87::obj-204::obj-138" : [ "vst~[100]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-47::obj-3" : [ "Wave[12]", "Wave", 0 ],
			"obj-87::obj-3944::obj-134" : [ "vst~[3]", "vst~", 0 ],
			"obj-87::obj-4076::obj-133" : [ "vst~[50]", "vst~", 0 ],
			"obj-87::obj-2::obj-5::obj-47::obj-81" : [ "Bank[28]", "Bank", 0 ],
			"obj-87::obj-3486::obj-102::obj-47::obj-110" : [ "Wavetable[12]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-102::obj-29::obj-110" : [ "Wavetable[16]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-102::obj-26::obj-110" : [ "Wavetable[17]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-5::obj-29::obj-12" : [ "Mute[6]", "Mute", 0 ],
			"obj-87::obj-3999::obj-22::obj-9" : [ "live.dial[5]", "live.dial", 0 ],
			"obj-87::obj-2::obj-102::obj-48::obj-12" : [ "Mute[21]", "Mute", 0 ],
			"obj-87::obj-292::obj-22::obj-9" : [ "live.dial[16]", "live.dial", 0 ],
			"obj-87::obj-259::obj-133" : [ "vst~[78]", "vst~", 0 ],
			"obj-87::obj-204::obj-133" : [ "vst~[102]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-29::obj-12" : [ "Mute[16]", "Mute", 0 ],
			"obj-87::obj-3486::obj-5::obj-49::obj-110" : [ "Wavetable[10]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-5::obj-27::obj-12" : [ "Mute[2]", "Mute", 0 ],
			"obj-87::obj-3944::obj-133" : [ "vst~[2]", "vst~", 0 ],
			"obj-87::obj-4065::obj-138" : [ "vst~[44]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-29::obj-110" : [ "Wavetable[24]", "Wavetable", 0 ],
			"obj-87::obj-2::obj-102::obj-26::obj-3" : [ "Wave[25]", "Wave", 0 ],
			"obj-87::obj-3486::obj-102::obj-47::obj-81" : [ "Bank[12]", "Bank", 0 ],
			"obj-87::obj-3486::obj-102::obj-29::obj-81" : [ "Bank[16]", "Bank", 0 ],
			"obj-87::obj-3988::obj-22::obj-9" : [ "live.dial[4]", "live.dial", 0 ],
			"obj-87::obj-248::obj-134" : [ "vst~[81]", "vst~", 0 ],
			"obj-87::obj-182::obj-132" : [ "vst~[107]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-46::obj-12" : [ "Mute[7]", "Mute", 0 ],
			"obj-87::obj-3486::obj-5::obj-49::obj-81" : [ "Bank[10]", "Bank", 0 ],
			"obj-87::obj-3486::obj-5::obj-28::obj-154" : [ "CV2[5]", "CV2", 0 ],
			"obj-87::obj-3944::obj-132" : [ "vst~[1]", "vst~", 0 ],
			"obj-87::obj-4054::obj-138" : [ "vst~[40]", "vst~", 0 ],
			"obj-87::obj-2::obj-5::obj-27::obj-3" : [ "Wave[34]", "Wave", 0 ],
			"obj-87::obj-215::obj-132" : [ "vst~[87]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-47::obj-12" : [ "Mute[8]", "Mute", 0 ],
			"obj-87::obj-3977::obj-22::obj-9" : [ "live.dial[3]", "live.dial", 0 ],
			"obj-87::obj-2::obj-102::obj-47::obj-110" : [ "Wavetable[20]", "Wavetable", 0 ],
			"obj-87::obj-2::obj-5::obj-48::obj-81" : [ "Bank[29]", "Bank", 0 ],
			"obj-87::obj-2::obj-5::obj-28::obj-3" : [ "Wave[31]", "Wave", 0 ],
			"obj-87::obj-3486::obj-102::obj-46::obj-154" : [ "CV2[11]", "CV2", 0 ],
			"obj-87::obj-3486::obj-5::obj-28::obj-100" : [ "Offset[5]", "Offset", 0 ],
			"obj-87::obj-4043::obj-132" : [ "vst~[39]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-28::obj-100" : [ "Offset[23]", "Offset", 0 ],
			"obj-87::obj-2::obj-5::obj-48::obj-110" : [ "Wavetable[29]", "Wavetable", 0 ],
			"obj-87::obj-2::obj-5::obj-49::obj-100" : [ "Offset[30]", "Offset", 0 ],
			"obj-87::obj-281::obj-22::obj-9" : [ "live.dial[17]", "live.dial", 0 ],
			"obj-87::obj-193::obj-134" : [ "vst~[97]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-48::obj-12" : [ "Mute[13]", "Mute", 0 ],
			"obj-87::obj-3966::obj-22::obj-9" : [ "live.dial[2]", "live.dial", 0 ],
			"obj-87::obj-2::obj-102::obj-46::obj-81" : [ "Bank[19]", "Bank", 0 ],
			"obj-87::obj-2::obj-5::obj-46::obj-12" : [ "Mute[27]", "Mute", 0 ],
			"obj-87::obj-3486::obj-5::obj-27::obj-100" : [ "Offset[2]", "Offset", 0 ],
			"obj-87::obj-4032::obj-133" : [ "vst~[34]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-49::obj-110" : [ "Wavetable[22]", "Wavetable", 0 ],
			"obj-87::obj-2::obj-5::obj-49::obj-81" : [ "Bank[30]", "Bank", 0 ],
			"obj-87::obj-2::obj-5::obj-26::obj-81" : [ "Bank[33]", "Bank", 0 ],
			"obj-87::obj-281::obj-134" : [ "vst~[69]", "vst~", 0 ],
			"obj-87::obj-50::obj-22::obj-9" : [ "live.dial[27]", "live.dial", 0 ],
			"obj-87::obj-3955::obj-22::obj-9" : [ "live.dial[1]", "live.dial", 0 ],
			"obj-87::obj-4088::obj-133" : [ "vst~[54]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-46::obj-12" : [ "Mute[19]", "Mute", 0 ],
			"obj-87::obj-2::obj-102::obj-27::obj-12" : [ "Mute[26]", "Mute", 0 ],
			"obj-87::obj-50::obj-132" : [ "vst~[111]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-46::obj-110" : [ "Wavetable[11]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-5::obj-29::obj-3" : [ "Wave[6]", "Wave", 0 ],
			"obj-87::obj-4021::obj-138" : [ "vst~[28]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-49::obj-154" : [ "CV2[22]", "CV2", 0 ],
			"obj-87::obj-2::obj-5::obj-48::obj-3" : [ "Wave[29]", "Wave", 0 ],
			"obj-87::obj-2::obj-5::obj-29::obj-154" : [ "CV2[32]", "CV2", 0 ],
			"obj-87::obj-314::obj-133" : [ "vst~[58]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-46::obj-12" : [ "Mute[11]", "Mute", 0 ],
			"obj-87::obj-3486::obj-102::obj-29::obj-3" : [ "Wave[16]", "Wave", 0 ],
			"obj-87::obj-3486::obj-102::obj-27::obj-81" : [ "Bank[18]", "Bank", 0 ],
			"obj-87::obj-3486::obj-5::obj-27::obj-3" : [ "Wave[2]", "Wave", 0 ],
			"obj-87::obj-4076::obj-138" : [ "vst~[48]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-26::obj-12" : [ "Mute[25]", "Mute", 0 ],
			"obj-87::obj-2::obj-102::obj-27::obj-3" : [ "Wave[26]", "Wave", 0 ],
			"obj-87::obj-2::obj-5::obj-47::obj-100" : [ "Offset[28]", "Offset", 0 ],
			"obj-87::obj-237::obj-133" : [ "vst~[94]", "vst~", 0 ],
			"obj-87::obj-4010::obj-132" : [ "vst~[27]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-48::obj-154" : [ "CV2[21]", "CV2", 0 ],
			"obj-87::obj-303::obj-134" : [ "vst~[61]", "vst~", 0 ],
			"obj-87::obj-182::obj-134" : [ "vst~[105]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-46::obj-3" : [ "Wave[7]", "Wave", 0 ],
			"obj-87::obj-3486::obj-5::obj-29::obj-110" : [ "Wavetable[6]", "Wavetable", 0 ],
			"obj-87::obj-4065::obj-134" : [ "vst~[45]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-29::obj-81" : [ "Bank[24]", "Bank", 0 ],
			"obj-87::obj-2::obj-102::obj-26::obj-110" : [ "Wavetable[25]", "Wavetable", 0 ],
			"obj-87::obj-193::obj-22::obj-9" : [ "live.dial[24]", "live.dial", 0 ],
			"obj-87::obj-3999::obj-138" : [ "vst~[20]", "vst~", 0 ],
			"obj-87::obj-292::obj-138" : [ "vst~[64]", "vst~", 0 ],
			"obj-87::obj-259::obj-132" : [ "vst~[79]", "vst~", 0 ],
			"obj-87::obj-182::obj-133" : [ "vst~[106]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-29::obj-81" : [ "Bank[6]", "Bank", 0 ],
			"obj-87::obj-3486::obj-5::obj-26::obj-154" : [ "CV2[4]", "CV2", 0 ],
			"obj-87::obj-4054::obj-132" : [ "vst~[43]", "vst~", 0 ],
			"obj-87::obj-2::obj-5::obj-49::obj-110" : [ "Wavetable[30]", "Wavetable", 0 ],
			"obj-87::obj-2::obj-5::obj-27::obj-100" : [ "Offset[34]", "Offset", 0 ],
			"obj-87::obj-215::obj-133" : [ "vst~[86]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-48::obj-81" : [ "Bank[13]", "Bank", 0 ],
			"obj-87::obj-3486::obj-102::obj-48::obj-3" : [ "Wave[13]", "Wave", 0 ],
			"obj-87::obj-3486::obj-102::obj-28::obj-100" : [ "Offset[15]", "Offset", 0 ],
			"obj-87::obj-3486::obj-5::obj-46::obj-110" : [ "Wavetable[7]", "Wavetable", 0 ],
			"obj-87::obj-3988::obj-133" : [ "vst~[18]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-47::obj-12" : [ "Mute[20]", "Mute", 0 ],
			"obj-87::obj-2::obj-5::obj-28::obj-154" : [ "CV2[31]", "CV2", 0 ],
			"obj-87::obj-248::obj-133" : [ "vst~[82]", "vst~", 0 ],
			"obj-87::obj-50::obj-138" : [ "vst~[108]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-26::obj-100" : [ "Offset[4]", "Offset", 0 ],
			"obj-87::obj-4043::obj-134" : [ "vst~[37]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-28::obj-12" : [ "Mute[23]", "Mute", 0 ],
			"obj-87::obj-2::obj-5::obj-29::obj-12" : [ "Mute[32]", "Mute", 0 ],
			"obj-87::obj-193::obj-138" : [ "vst~[96]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-46::obj-81" : [ "Bank[7]", "Bank", 0 ],
			"obj-87::obj-3486::obj-5::obj-47::obj-154" : [ "CV2[8]", "CV2", 0 ],
			"obj-87::obj-3486::obj-5::obj-27::obj-154" : [ "CV2[2]", "CV2", 0 ],
			"obj-87::obj-3977::obj-138" : [ "vst~[12]", "vst~", 0 ],
			"obj-87::obj-2::obj-5::obj-46::obj-100" : [ "Offset[27]", "Offset", 0 ],
			"obj-87::obj-50::obj-134" : [ "vst~[109]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-48::obj-154" : [ "CV2[9]", "CV2", 0 ],
			"obj-87::obj-4032::obj-134" : [ "vst~[33]", "vst~", 0 ],
			"obj-87::obj-2::obj-5::obj-49::obj-154" : [ "CV2[30]", "CV2", 0 ],
			"obj-87::obj-2::obj-5::obj-26::obj-110" : [ "Wavetable[33]", "Wavetable", 0 ],
			"obj-87::obj-204::obj-22::obj-9" : [ "live.dial[25]", "live.dial", 0 ],
			"obj-87::obj-3486::obj-102::obj-27::obj-110" : [ "Wavetable[18]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-5::obj-47::obj-100" : [ "Offset[8]", "Offset", 0 ],
			"obj-87::obj-3966::obj-138" : [ "vst~[8]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-46::obj-100" : [ "Offset[19]", "Offset", 0 ],
			"obj-87::obj-3486::obj-5::obj-48::obj-100" : [ "Offset[9]", "Offset", 0 ],
			"obj-87::obj-4021::obj-133" : [ "vst~[30]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-49::obj-3" : [ "Wave[22]", "Wave", 0 ],
			"obj-87::obj-2::obj-5::obj-48::obj-100" : [ "Offset[29]", "Offset", 0 ],
			"obj-87::obj-270::obj-138" : [ "vst~[72]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-28::obj-12" : [ "Mute[15]", "Mute", 0 ],
			"obj-87::obj-3955::obj-132" : [ "vst~[7]", "vst~", 0 ],
			"obj-87::obj-4088::obj-138" : [ "vst~[52]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-27::obj-154" : [ "CV2[26]", "CV2", 0 ],
			"obj-87::obj-3486::obj-102::obj-49::obj-154" : [ "CV2[14]", "CV2", 0 ],
			"obj-87::obj-3486::obj-5::obj-26::obj-12" : [ "Mute[4]", "Mute", 0 ],
			"obj-87::obj-4010::obj-138" : [ "vst~[24]", "vst~", 0 ],
			"obj-87::obj-314::obj-132" : [ "vst~[59]", "vst~", 0 ],
			"obj-87::obj-270::obj-132" : [ "vst~[75]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-28::obj-110" : [ "Wavetable[5]", "Wavetable", 0 ],
			"obj-87::obj-4076::obj-134" : [ "vst~[49]", "vst~", 0 ],
			"obj-87::obj-2::obj-5::obj-47::obj-3" : [ "Wave[28]", "Wave", 0 ],
			"obj-87::obj-3486::obj-102::obj-46::obj-81" : [ "Bank[11]", "Bank", 0 ],
			"obj-87::obj-3486::obj-102::obj-46::obj-3" : [ "Wave[11]", "Wave", 0 ],
			"obj-87::obj-3486::obj-102::obj-49::obj-100" : [ "Offset[14]", "Offset", 0 ],
			"obj-87::obj-3944::obj-22::obj-9" : [ "live.dial", "live.dial", 0 ],
			"obj-87::obj-3999::obj-134" : [ "vst~[21]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-48::obj-110" : [ "Wavetable[21]", "Wavetable", 0 ],
			"obj-87::obj-303::obj-133" : [ "vst~[62]", "vst~", 0 ],
			"obj-87::obj-204::obj-134" : [ "vst~[101]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-26::obj-12" : [ "Mute[17]", "Mute", 0 ],
			"obj-87::obj-3486::obj-5::obj-28::obj-81" : [ "Bank[5]", "Bank", 0 ],
			"obj-87::obj-4065::obj-133" : [ "vst~[46]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-29::obj-12" : [ "Mute[24]", "Mute", 0 ],
			"obj-87::obj-3486::obj-5::obj-48::obj-12" : [ "Mute[9]", "Mute", 0 ],
			"obj-87::obj-3988::obj-132" : [ "vst~[19]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-47::obj-100" : [ "Offset[20]", "Offset", 0 ],
			"obj-87::obj-226::obj-134" : [ "vst~[89]", "vst~", 0 ],
			"obj-87::obj-182::obj-22::obj-9" : [ "live.dial[26]", "live.dial", 0 ],
			"obj-87::obj-4054::obj-133" : [ "vst~[42]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-28::obj-110" : [ "Wavetable[23]", "Wavetable", 0 ],
			"obj-87::obj-2::obj-5::obj-29::obj-100" : [ "Offset[32]", "Offset", 0 ],
			"obj-87::obj-2::obj-5::obj-27::obj-110" : [ "Wavetable[34]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-102::obj-26::obj-154" : [ "CV2[17]", "CV2", 0 ],
			"obj-87::obj-3977::obj-134" : [ "vst~[13]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-47::obj-3" : [ "Wave[20]", "Wave", 0 ],
			"obj-87::obj-2::obj-5::obj-46::obj-3" : [ "Wave[27]", "Wave", 0 ],
			"obj-87::obj-2::obj-5::obj-28::obj-100" : [ "Offset[31]", "Offset", 0 ],
			"obj-87::obj-4043::obj-133" : [ "vst~[38]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-28::obj-81" : [ "Bank[23]", "Bank", 0 ],
			"obj-87::obj-2::obj-5::obj-49::obj-3" : [ "Wave[30]", "Wave", 0 ],
			"obj-87::obj-2::obj-5::obj-29::obj-3" : [ "Wave[32]", "Wave", 0 ],
			"obj-87::obj-2::obj-5::obj-26::obj-154" : [ "CV2[33]", "CV2", 0 ],
			"obj-87::obj-292::obj-132" : [ "vst~[67]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-28::obj-3" : [ "Wave[15]", "Wave", 0 ],
			"obj-87::obj-3966::obj-134" : [ "vst~[9]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-46::obj-3" : [ "Wave[19]", "Wave", 0 ],
			"obj-87::obj-2::obj-5::obj-46::obj-154" : [ "CV2[27]", "CV2", 0 ],
			"obj-87::obj-2::obj-5::obj-28::obj-12" : [ "Mute[31]", "Mute", 0 ],
			"obj-87::obj-50::obj-133" : [ "vst~[110]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-28::obj-110" : [ "Wavetable[15]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-5::obj-26::obj-3" : [ "Wave[4]", "Wave", 0 ],
			"obj-87::obj-4032::obj-138" : [ "vst~[32]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-49::obj-12" : [ "Mute[22]", "Mute", 0 ],
			"obj-87::obj-2::obj-5::obj-26::obj-100" : [ "Offset[33]", "Offset", 0 ],
			"obj-87::obj-314::obj-22::obj-9" : [ "live.dial[14]", "live.dial", 0 ],
			"obj-87::obj-281::obj-133" : [ "vst~[70]", "vst~", 0 ],
			"obj-87::obj-193::obj-133" : [ "vst~[98]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-49::obj-12" : [ "Mute[14]", "Mute", 0 ],
			"obj-87::obj-3955::obj-134" : [ "vst~[5]", "vst~", 0 ],
			"obj-87::obj-4088::obj-132" : [ "vst~[55]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-27::obj-110" : [ "Wavetable[26]", "Wavetable", 0 ],
			"obj-87::obj-237::obj-134" : [ "vst~[93]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-28::obj-81" : [ "Bank[15]", "Bank", 0 ],
			"obj-87::obj-3486::obj-102::obj-27::obj-12" : [ "Mute[18]", "Mute", 0 ],
			"obj-87::obj-4021::obj-134" : [ "vst~[29]", "vst~", 0 ],
			"obj-87::obj-2::obj-5::obj-29::obj-81" : [ "Bank[32]", "Bank", 0 ],
			"obj-87::obj-259::obj-22::obj-9" : [ "live.dial[19]", "live.dial", 0 ],
			"obj-87::obj-3486::obj-102::obj-26::obj-3" : [ "Wave[17]", "Wave", 0 ],
			"obj-87::obj-3486::obj-5::obj-47::obj-3" : [ "Wave[8]", "Wave", 0 ],
			"obj-87::obj-3486::obj-5::obj-26::obj-110" : [ "Wavetable[4]", "Wavetable", 0 ],
			"obj-87::obj-4076::obj-132" : [ "vst~[51]", "vst~", 0 ],
			"obj-87::obj-2::obj-5::obj-47::obj-12" : [ "Mute[28]", "Mute", 0 ],
			"obj-87::obj-215::obj-134" : [ "vst~[85]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-48::obj-3" : [ "Wave[9]", "Wave", 0 ],
			"obj-87::obj-4010::obj-133" : [ "vst~[26]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-48::obj-3" : [ "Wave[21]", "Wave", 0 ],
			"obj-87::obj-259::obj-134" : [ "vst~[77]", "vst~", 0 ],
			"obj-87::obj-204::obj-132" : [ "vst~[103]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-26::obj-81" : [ "Bank[4]", "Bank", 0 ],
			"obj-87::obj-3486::obj-5::obj-27::obj-110" : [ "Wavetable[2]", "Wavetable", 0 ],
			"obj-87::obj-4065::obj-132" : [ "vst~[47]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-29::obj-154" : [ "CV2[24]", "CV2", 0 ],
			"obj-87::obj-2::obj-102::obj-26::obj-100" : [ "Offset[25]", "Offset", 0 ],
			"obj-87::obj-226::obj-22::obj-9" : [ "live.dial[22]", "live.dial", 0 ],
			"obj-87::obj-3486::obj-5::obj-47::obj-110" : [ "Wavetable[8]", "Wavetable", 0 ],
			"obj-87::obj-3486::obj-5::obj-28::obj-12" : [ "Mute[5]", "Mute", 0 ],
			"obj-87::obj-3999::obj-133" : [ "vst~[22]", "vst~", 0 ],
			"obj-87::obj-292::obj-134" : [ "vst~[65]", "vst~", 0 ],
			"obj-87::obj-248::obj-138" : [ "vst~[80]", "vst~", 0 ],
			"obj-87::obj-182::obj-138" : [ "vst~[104]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-27::obj-100" : [ "Offset[18]", "Offset", 0 ],
			"obj-87::obj-3486::obj-5::obj-48::obj-110" : [ "Wavetable[9]", "Wavetable", 0 ],
			"obj-87::obj-4054::obj-134" : [ "vst~[41]", "vst~", 0 ],
			"obj-87::obj-2::obj-5::obj-27::obj-12" : [ "Mute[34]", "Mute", 0 ],
			"obj-87::obj-226::obj-138" : [ "vst~[88]", "vst~", 0 ],
			"obj-87::obj-3486::obj-102::obj-47::obj-154" : [ "CV2[12]", "CV2", 0 ],
			"obj-87::obj-3486::obj-102::obj-29::obj-154" : [ "CV2[16]", "CV2", 0 ],
			"obj-87::obj-3486::obj-5::obj-47::obj-81" : [ "Bank[8]", "Bank", 0 ],
			"obj-87::obj-3486::obj-5::obj-27::obj-81" : [ "Bank[2]", "Bank", 0 ],
			"obj-87::obj-3988::obj-134" : [ "vst~[17]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-47::obj-81" : [ "Bank[20]", "Bank", 0 ],
			"obj-87::obj-2::obj-5::obj-28::obj-81" : [ "Bank[31]", "Bank", 0 ],
			"obj-87::obj-248::obj-132" : [ "vst~[83]", "vst~", 0 ],
			"obj-87::obj-226::obj-133" : [ "vst~[90]", "vst~", 0 ],
			"obj-87::obj-3486::obj-5::obj-48::obj-81" : [ "Bank[9]", "Bank", 0 ],
			"obj-87::obj-3486::obj-5::obj-49::obj-154" : [ "CV2[10]", "CV2", 0 ],
			"obj-87::obj-4043::obj-138" : [ "vst~[36]", "vst~", 0 ],
			"obj-87::obj-2::obj-102::obj-28::obj-154" : [ "CV2[23]", "CV2", 0 ],
			"obj-87::obj-2::obj-5::obj-48::obj-154" : [ "CV2[29]", "CV2", 0 ],
			"obj-87::obj-292::obj-133" : [ "vst~[66]", "vst~", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "audioTrack_reduced.maxpat",
				"bootpath" : "H:/My Drive/CT7MT/Demo/Demo_R10",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "effectsRack.maxpat",
				"bootpath" : "H:/My Drive/CT7MT/Demo/Demo_R10",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "audioSources.maxpat",
				"bootpath" : "H:/My Drive/CT7MT/Demo/Demo_R10",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "quadSequencer.maxpat",
				"bootpath" : "H:/My Drive/CT7MT/Demo/Demo_R10",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
